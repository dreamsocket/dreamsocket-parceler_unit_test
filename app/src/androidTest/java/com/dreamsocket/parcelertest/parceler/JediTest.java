package com.dreamsocket.parcelertest.parceler;

import android.app.Application;
import android.os.Parcel;
import android.os.Parcelable;
import android.test.ApplicationTestCase;
import android.util.Log;

import org.junit.Test;
import org.parceler.Parcels;

import java.util.HashMap;

public class JediTest extends ApplicationTestCase<Application> {
    public static final String TAG = JediTest.class.getSimpleName();

    public JediTest() {
        super(Application.class);
    }


    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }


    @Test
    public void testParceler() throws Exception {
        JediCouncil aJediCouncil = new JediCouncil("Coruscant");

        HashMap<String, Jedi> forceSensitive = new HashMap<>();

        forceSensitive.put("Yoda", this.createJedi("green", 1L, "unknow", true, false, false, "", "Yoda"));
        forceSensitive.put("Obi-wan", this.createJedi("blue", 2L,"Coruscant", true, false, false, "Kwi-gon Jinn", "Obi-wan Kenobi"));
        forceSensitive.put("Anakin", this.createJedi("blue", 3L, "Tatooine", true, false, false, "Obi-wan Kenobi", "Anakin Skywalker"));
        forceSensitive.put("Luke", this.createJedi("blue", 4L, "Tatooine", true, false, false, "Yoda", "Luke Skywalker"));
        forceSensitive.put("Rey", this.createJedi("blue", 5L, "Jakku", false, true, false, "Luke Skywalker", "Rey Solo"));

        forceSensitive.put("Snoke", this.createJedi("red", 6L, "unknown", true, false, false, "", "Supreme Leader Snoke"));
        forceSensitive.put("Darth Sidious", this.createJedi("red", 7L, "Naboo", true, false, true, "Darth Plagueis", "Darth Sidious"));
        forceSensitive.put("Darth Maul", this.createJedi("red", 8L, "Talzin", false, false, true, "Darth Sidious", "Darth Maul"));
        forceSensitive.put("Darth Vader", this.createJedi("red", 9L, "Tatooine", false, false, true, "Darth Sidious", "Darth Vader"));
        forceSensitive.put("Ren", this.createJedi("red", 10L, "Coruscant", false, true, false, "Supreme Leader Snoke", "Kylo Ren"));

        aJediCouncil.setTestData(forceSensitive);

        Parcelable wrapped = Parcels.wrap(aJediCouncil);
        Parcel parcel = Parcel.obtain();
        wrapped.writeToParcel(parcel, 0);
        parcel.setDataPosition(0);

        /// pulling from created parcel...
        Parcelable inputParcelable = ((Parcelable.Creator<Parcelable>)wrapped.getClass().getField("CREATOR").get(null)).createFromParcel(parcel);
        JediCouncil bJediCouncil = Parcels.unwrap(inputParcelable);


        /// visual inspection of matching values
        Log.d(TAG, "aJediCouncil: " + aJediCouncil.toString());
        Log.d(TAG, "bJediCouncil: " + bJediCouncil.toString());

        // assertions
        assertTrue("The councils are NOT equal", aJediCouncil.equals(bJediCouncil));
        assertNotSame("The councils are equal", aJediCouncil == bJediCouncil);
    }


    protected Jedi createJedi(String p_bladeColor, Long p_id, String p_homePlanet, boolean p_hasPadawan, boolean p_isPadawan, boolean p_isSith, String p_master, String p_name) {
        Jedi jedi = new Jedi();
        jedi.bladeColor = p_bladeColor;
        jedi.id = p_id;
        jedi.homePlanet = p_homePlanet;
        jedi.hasPadawan = p_hasPadawan;
        jedi.isPadawan = p_isPadawan;
        jedi.isSith = p_isSith;
        jedi.master = p_master;
        jedi.name = p_name;

        return jedi;
    }


    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}