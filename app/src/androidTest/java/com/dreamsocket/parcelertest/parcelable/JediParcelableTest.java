package com.dreamsocket.parcelertest.parcelable;

import android.app.Application;
import android.os.Parcel;
import android.test.ApplicationTestCase;
import android.util.Log;

import org.junit.Test;


public class JediParcelableTest extends ApplicationTestCase<Application> {
    public static final String TAG = JediParcelableTest.class.getSimpleName();

    public JediParcelableTest() {
        super(Application.class);
    }


    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }


    @Test
    public void testJediParcelable() throws Exception {
        JediParcelable aJedi = new JediParcelable();

        aJedi.bladeColor = "blue";
        aJedi.id = 5L;
        aJedi.homePlanet = "Jakku";
        aJedi.hasPadawan = false;
        aJedi.isPadawan = true;
        aJedi.isSith = false;
        aJedi.master = "Luke Skywalker";
        aJedi.name = "Rey Solo";

        Parcel parcel = Parcel.obtain();
        aJedi.writeToParcel(parcel, 0);
        parcel.setDataPosition(0);
        JediParcelable bJedi = JediParcelable.CREATOR.createFromParcel(parcel);

        Log.d(TAG, "aJedi: " + aJedi.toString());
        Log.d(TAG, "bJedi: " + bJedi.toString());

        assertTrue("Jedi are not the same", aJedi.equals(bJedi) );
        assertNotSame("Jedi are same Object", aJedi == bJedi);
    }


    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}