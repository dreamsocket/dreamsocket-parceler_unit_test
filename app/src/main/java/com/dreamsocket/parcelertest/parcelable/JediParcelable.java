package com.dreamsocket.parcelertest.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

public class JediParcelable implements Parcelable {
    public String bladeColor;
    public Long id;
    public String homePlanet;
    public boolean hasPadawan;
    public boolean isPadawan;
    public boolean isSith;
    public String master;
    public String name;

    public JediParcelable(){}

    public static final Parcelable.Creator<JediParcelable> CREATOR = new Parcelable.Creator<JediParcelable>() {
        public JediParcelable createFromParcel(Parcel in) {
            return new JediParcelable(in);
        }

        public JediParcelable[] newArray(int size) {
            return new JediParcelable[size];
        }
    };

    private JediParcelable(Parcel p_in){
        this.bladeColor = p_in.readString();
        this.id = p_in.readLong();
        this.homePlanet = p_in.readString();
        this.hasPadawan = p_in.readInt() == 1;
        this.isPadawan = p_in.readInt() == 1;
        this.isSith = p_in.readInt() == 1;
        this.master = p_in.readString();
        this.name = p_in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.bladeColor);
        dest.writeLong(this.id);
        dest.writeString(this.homePlanet);
        dest.writeInt(this.hasPadawan ? 1 : 0);
        dest.writeInt(this.isPadawan ? 1 : 0);
        dest.writeInt(this.isSith ? 1 : 0);
        dest.writeString(this.master);
        dest.writeString(this.name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public boolean equals(Object m_p_o) {
        if (this == m_p_o) return true;
        if (!(m_p_o instanceof JediParcelable)) return false;

        JediParcelable m_that = (JediParcelable) m_p_o;

        if (hasPadawan != m_that.hasPadawan) return false;
        if (isPadawan != m_that.isPadawan) return false;
        if (isSith != m_that.isSith) return false;
        if (!bladeColor.equals(m_that.bladeColor)) return false;
        if (!id.equals(m_that.id)) return false;
        if (homePlanet != null ? !homePlanet.equals(m_that.homePlanet) : m_that.homePlanet != null) return false;
        if (master != null ? !master.equals(m_that.master) : m_that.master != null) return false;
        return name.equals(m_that.name);

    }

    @Override
    public String toString() {
        return "Jedi{" +
                "bladeColor='" + bladeColor + '\'' +
                ", id=" + id +
                ", homePlanet='" + homePlanet + '\'' +
                ", hasPadawan=" + hasPadawan +
                ", isPadawan=" + isPadawan +
                ", isSith=" + isSith +
                ", master='" + master + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}