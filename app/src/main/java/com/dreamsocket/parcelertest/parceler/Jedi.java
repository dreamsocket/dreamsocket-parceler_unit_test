package com.dreamsocket.parcelertest.parceler;

import org.parceler.Parcel;

@Parcel
public class Jedi {

    public String bladeColor;
    public Long id;
    public String homePlanet;
    public boolean hasPadawan;
    public boolean isPadawan;
    public boolean isSith;
    public String master;
    public String name;

    @Override
    public boolean equals(Object m_p_o) {
        if (this == m_p_o) return true;
        if (!(m_p_o instanceof Jedi)) return false;

        Jedi m_m_jedi = (Jedi) m_p_o;

        if (hasPadawan != m_m_jedi.hasPadawan) return false;
        if (isPadawan != m_m_jedi.isPadawan) return false;
        if (isSith != m_m_jedi.isSith) return false;
        if (bladeColor != null ? !bladeColor.equals(m_m_jedi.bladeColor) : m_m_jedi.bladeColor != null) return false;
        if (id != null ? !id.equals(m_m_jedi.id) : m_m_jedi.id != null) return false;
        if (homePlanet != null ? !homePlanet.equals(m_m_jedi.homePlanet) : m_m_jedi.homePlanet != null) return false;
        if (master != null ? !master.equals(m_m_jedi.master) : m_m_jedi.master != null) return false;
        return name != null ? name.equals(m_m_jedi.name) : m_m_jedi.name == null;

    }

    @Override
    public String toString() {
        return "Jedi{" +
                "bladeColor='" + bladeColor + '\'' +
                ", id=" + id +
                ", homePlanet='" + homePlanet + '\'' +
                ", hasPadawan=" + hasPadawan +
                ", isPadawan=" + isPadawan +
                ", isSith=" + isSith +
                ", master='" + master + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}