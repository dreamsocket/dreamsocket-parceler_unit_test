package com.dreamsocket.parcelertest.parceler;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by NeoRiley on 4/19/16.
 */

@Parcel
public class JediCouncil {
    public final static String TAG = JediCouncil.class.getSimpleName();

    public ArrayList<String> jediList;
    public String location;
    public int memberCount;
    public ArrayList<String> sithList;
    public HashMap<String, Jedi> forceSensitive;

    public JediCouncil() {/*Required empty bean constructor since we have another parameterized versin*/}

    public JediCouncil(String p_location) {
        this.location = p_location;
        this.forceSensitive = new HashMap<>();
        this.jediList = new ArrayList<>();
        this.sithList = new ArrayList<>();
    }


    public void setTestData(HashMap<String, Jedi> p_forceSensitive){
        this.forceSensitive = p_forceSensitive;

        for( Map.Entry<String, Jedi> entry : this.forceSensitive.entrySet() ){
            Jedi jedi = entry.getValue();
            if(jedi.isSith){
                this.sithList.add(jedi.name);
            } else {
                this.jediList.add(jedi.name);
            }
        }

        this.memberCount = this.forceSensitive.size();
    }


    @Override
    public boolean equals(Object m_p_o) {
        if (this == m_p_o) return true;

        if (!(m_p_o instanceof JediCouncil)) return false;

        JediCouncil m_that = (JediCouncil) m_p_o;

        if (memberCount != m_that.memberCount) return false;

        Collections.sort(jediList);
        Collections.sort(m_that.jediList);

        if (jediList != null ? !jediList.equals(m_that.jediList) : m_that.jediList != null) return false;
        if (!location.equals(m_that.location)) return false;
        if (sithList != null ? !sithList.equals(m_that.sithList) : m_that.sithList != null) return false;

        Map<String, Jedi>  tree_0 = new TreeMap<>(forceSensitive);
        Map<String, Jedi>  tree_1 = new TreeMap<>(m_that.forceSensitive);

        return forceSensitive != null ? tree_0.equals(tree_1) : m_that.forceSensitive == null;
    }


    @Override
    public int hashCode() {
        int m_result = jediList != null ? jediList.hashCode() : 0;
        m_result = 31 * m_result + location.hashCode();
        m_result = 31 * m_result + memberCount;
        m_result = 31 * m_result + (sithList != null ? sithList.hashCode() : 0);
        m_result = 31 * m_result + (forceSensitive != null ? forceSensitive.hashCode() : 0);
        return m_result;
    }

    @Override
    public String toString() {
        return "JediCouncil{" +
                "jediList=" + jediList +
                ", location='" + location + '\'' +
                ", memberCount=" + memberCount +
                ", sithList=" + sithList +
                ", forceSensitive=" + forceSensitive +
                '}';
    }
}
